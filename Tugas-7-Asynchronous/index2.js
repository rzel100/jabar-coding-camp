var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
];

// Soal 2
readBooksPromise(10000, books[0]).then(function(resolve) {
    readBooksPromise(resolve, books[1]).then(function(resolve) {
        readBooksPromise(resolve, books[2]).then(function(resolve) {
            readBooksPromise(resolve, books[3]);
        });
    });
});