// Soal 1

function jumlah_kata(data) {
	var theKata = data.split(' ');
	for (i = 0;i < theKata.length;i++) {
		if (theKata[i].length == 0) {
			theKata.splice(i, 1);
			i--;
		};
	};
	return theKata.length;
};

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";
//var kalimat_4 = "    Halo   Nama   Saya   Mohamad Arizal Hadisucipto   :)   "

console.log( jumlah_kata(kalimat_1) ); // 6
console.log( jumlah_kata(kalimat_2) ); // 2
console.log( jumlah_kata(kalimat_3) ); // 4
//console.log( jumlah_kata(kalimat_4) ); // 7

// Soal 2

function next_date(tanggalnya, bulannya, tahunnya) {
	var namaBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
	var jumlahHari = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; // Sumber Wikipedia
	if (tahunnya % 4 == 0) {
		jumlahHari[1]++;
	};
	tanggalnya += 1;
	if (tanggalnya > jumlahHari[bulannya-1]) {
		tanggalnya = 1;
		bulannya += 1;
		if (bulannya > namaBulan.length) {
			bulannya = 1;
			tahunnya += 1;
		};
	};
	var tanggalBerikutnya = ''+ tanggalnya + ' ' + namaBulan[bulannya-1] + ' ' + tahunnya +''
	return tanggalBerikutnya;
};

var tanggal = 29
var bulan = 2
var tahun = 2020

console.log( next_date(tanggal , bulan , tahun ) ) // output : 1 Maret 2020

var tanggal = 28
var bulan = 2
var tahun = 2021

console.log( next_date(tanggal , bulan , tahun ) ) // output : 1 Maret 2021

var tanggal = 31
var bulan = 12
var tahun = 2020

console.log( next_date(tanggal , bulan , tahun ) ) // output : 1 Januari 2021

var tanggal = 22
var bulan = 7
var tahun = 2077

console.log( next_date(tanggal , bulan , tahun ) ) // output : 23 Juli 2077