// Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

for (i = 0; i < daftarHewan.length; i++) {
	console.log(daftarHewan[i])
};

// Soal 2

function introduce(datanya) {
	return "Nama saya "+datanya.name+", umur saya "+datanya.age+" tahun, alamat saya di "+datanya.address+", dan saya punya hobby yaitu "+datanya.hobby;
};

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3

function hitung_huruf_vokal(data) {
	var hurufVokal = ['a', 'i', 'u', 'e', 'o'];
	var jumlahVokal = 0;
	for (i = 0; i < data.length; i++) {
		for (j = 0; j < hurufVokal.length; j++) {
			if (data[i].toLowerCase() == hurufVokal[j]) {
				jumlahVokal++;
			};
		};
	};
	return jumlahVokal;
};

var hitung_1 = hitung_huruf_vokal("Mohamad Arizal");

var hitung_2 = hitung_huruf_vokal("Hadisucipto");

console.log(hitung_1 , hitung_2); // 6 5

// Soal 4

function hitung(data) {
	var hasilHitung = (data - 1) * 2;
	return hasilHitung;
};

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8