// Soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// Output Harus = "saya senang belajar JAVASCRIPT"
var hasilnya = pertama.substr(0, 5) + pertama.substr(12, 7) + kedua.substr(0, 8) + kedua.substr(8, 10).toUpperCase();
console.log(hasilnya);

// Soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//Output Harus = 24 (Int)
var hasilnya = ( Number(kataPertama) + Number(kataKeempat) ) + ( Number(kataKedua) * Number(kataKetiga) );
console.log(hasilnya);

// Soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);

/*
Output Harus :
	Kata Pertama: wah
	Kata Kedua: javascript
	Kata Ketiga: itu
	Kata Keempat: keren
	Kata Kelima: sekali
*/
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima)